package me.jasperandrew.nondescriptgame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;
    private BlockManager blockManager;

    public GameView(Context context) {
        super(context);
        getHolder().addCallback(this);

        thread = new MainThread(getHolder(), this);

        blockManager = new BlockManager(700, 70);
        blockManager.init();

        setFocusable(true);
    }

    public void update() {
        if(!Const.GAME_OVER) {
            blockManager.update();
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if(!Const.GAME_OVER){
            canvas.drawColor(Color.BLACK);
            blockManager.draw(canvas);
        }else{
            GameOverScreen gameOverScreen = new GameOverScreen();
            gameOverScreen.draw(canvas);
        }
    }

    private void resetGame() {
        Const.GAME_OVER = false;
        Const.SCORE = 0;
        blockManager.init();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                if(Const.GAME_OVER){
                    resetGame();
                    return true;
                }
                blockManager.placeBlock();
        }
        return true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread = new MainThread(getHolder(), this);
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while(retry){
            try {
                thread.setRunning(false);
                thread.join();
            } catch(Exception e) { e.printStackTrace(); }

            retry = false;
        }
    }

}

