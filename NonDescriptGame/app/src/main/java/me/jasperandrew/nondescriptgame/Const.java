package me.jasperandrew.nondescriptgame;

class Const {
    static int SCREEN_WIDTH;
    static int SCREEN_HEIGHT;
    static final int MAX_FPS = 60;

    static float MOVESPEED= 11.0f;

    static int SCORE;

    static boolean GAME_OVER = false;
}