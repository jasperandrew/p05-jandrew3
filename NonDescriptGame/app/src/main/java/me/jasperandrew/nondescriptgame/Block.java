package me.jasperandrew.nondescriptgame;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

class Block {
    private Rect rect;
    private boolean moving;
    private int velocity;

    Block(int startX, int startY, int width, int height, boolean moving) {
        this.moving = moving;

        rect = new Rect(startX, startY, startX+width, startY+height);

        velocity = (int)((Math.random() * (3 + 3) + 1)) + 3;
    }

    Rect getRect() {
        return rect;
    }

    boolean isMoving() {
        return moving;
    }

    void stopMoving() {
        moving = false;
    }

    void setRectR(int x) {
        rect.right = x;
    }

    void setRectL(int x) {
        rect.left = x;
    }

    void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.GRAY);
        canvas.drawRect(rect, paint);
    }

    void update(boolean slideScreen) {
        if(moving){
            rect.left += velocity;
            rect.right += velocity;

            if(rect.left < 1 || rect.right > Const.SCREEN_WIDTH-1) velocity *= -1;
        }
        if(slideScreen){
            rect.top += Const.MOVESPEED;
            rect.bottom += Const.MOVESPEED;
        }
    }
}

