package me.jasperandrew.nondescriptgame;

import android.graphics.Canvas;
import android.graphics.Rect;

import java.util.ArrayList;

class BlockManager {
    private ArrayList<Block> blocks;
    private int width;
    private int height;
    private int currWidth;

    BlockManager(int width, int height) {
        blocks = new ArrayList<>();
        this.width = width;
        this.height = height;
        this.currWidth = width;
    }

    void init() {
        blocks.clear();
        currWidth = width;
        blocks.add(new Block(Const.SCREEN_WIDTH/2-width/2, Const.SCREEN_HEIGHT-height, width, height, true));
    }

    void placeBlock() {
        blocks.get(blocks.size()-1).stopMoving();
    }

    void update() {
        if(blocks.get(0).getRect().top > Const.SCREEN_HEIGHT) {
            blocks.remove(0);
        }

        if(!blocks.get(blocks.size()-1).isMoving()){
            if(blocks.size() > 1){
                Rect block1 = blocks.get(blocks.size()-1).getRect();
                Rect block2 = blocks.get(blocks.size()-2).getRect();

                if((block1.right > block2.right && block1.left >= block2.right) || (block1.left < block2.left && block1.right <= block2.left)) {
                    Const.GAME_OVER = true;
                    return;
                }else{
                    if(block1.right > block2.right)
                        blocks.get(blocks.size()-1).setRectR(block2.right);
                    if(block1.left < block2.left)
                        blocks.get(blocks.size()-1).setRectL(block2.left);


                    currWidth = blocks.get(blocks.size()-1).getRect().width();
                }
            }

            blocks.add(new Block(Const.SCREEN_WIDTH/2-width/2, blocks.get(blocks.size()-1).getRect().top-height, currWidth, height, true));
            Const.SCORE += 1;
        }

        boolean slideScreen = false;
        if(blocks.get(blocks.size()-1).getRect().top < Const.SCREEN_HEIGHT/5)
            slideScreen = true;

        for(Block block : blocks){
            block.update(slideScreen);
        }
    }

    void draw(Canvas canvas) {
        for(Block block : blocks)
            block.draw(canvas);
    }
}
